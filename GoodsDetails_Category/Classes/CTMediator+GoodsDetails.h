//
//  CTMediator+GoodsDetails.h
//  FengLei
//
//  Created by liaoxf on 2018/5/21.
//  Copyright © 2018年 com.mlj.FengLei. All rights reserved.
//

#import <CTMediator/CTMediator.h>

@interface CTMediator (GoodsDetails)

- (UIViewController *)goodsDetails_viewControllerWithParams:(NSDictionary *)params;

@end
