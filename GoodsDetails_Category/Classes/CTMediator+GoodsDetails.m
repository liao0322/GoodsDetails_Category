//
//  CTMediator+GoodsDetails.m
//  FengLei
//
//  Created by liaoxf on 2018/5/21.
//  Copyright © 2018年 com.mlj.FengLei. All rights reserved.
//

#import "CTMediator+GoodsDetails.h"

NSString * const kCTMediatorTarget_goodsDetails = @"GoodsDetails";
NSString * const kCTMediatorActionNativTo_GoodDetalViewController = @"NativeToGoodsDetailsViewController";

@implementation CTMediator (GoodsDetails)

- (UIViewController *)goodsDetails_viewControllerWithParams:(NSDictionary *)params {
    UIViewController *viewController = [self performTarget:kCTMediatorTarget_goodsDetails action:kCTMediatorActionNativTo_GoodDetalViewController params:params shouldCacheTarget:NO];
    
    if ([viewController isKindOfClass:[UIViewController class]]) {
        return viewController;
    } else {
        NSLog(@"%@ 未能实例化页面", NSStringFromSelector(_cmd));
        return [[UIViewController alloc] init];
    }
}

@end
