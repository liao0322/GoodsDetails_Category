# GoodsDetails_Category

[![CI Status](https://img.shields.io/travis/liao0322/GoodsDetails_Category.svg?style=flat)](https://travis-ci.org/liao0322/GoodsDetails_Category)
[![Version](https://img.shields.io/cocoapods/v/GoodsDetails_Category.svg?style=flat)](https://cocoapods.org/pods/GoodsDetails_Category)
[![License](https://img.shields.io/cocoapods/l/GoodsDetails_Category.svg?style=flat)](https://cocoapods.org/pods/GoodsDetails_Category)
[![Platform](https://img.shields.io/cocoapods/p/GoodsDetails_Category.svg?style=flat)](https://cocoapods.org/pods/GoodsDetails_Category)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GoodsDetails_Category is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GoodsDetails_Category'
```

## Author

liao0322, liao0322@hotmail.com

## License

GoodsDetails_Category is available under the MIT license. See the LICENSE file for more info.
