//
//  main.m
//  GoodsDetails_Category
//
//  Created by liao0322 on 05/21/2018.
//  Copyright (c) 2018 liao0322. All rights reserved.
//

@import UIKit;
#import "FLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([FLAppDelegate class]));
    }
}
